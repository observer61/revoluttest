package revoluttest.model.factory;

import com.google.inject.Inject;
import com.google.inject.Provider;
import revoluttest.model.Account;

import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class AccountFactory {

    private final AtomicLong idGenerator;
    private final Provider<ReentrantReadWriteLock> lockProvider;

    @Inject
    public AccountFactory(AtomicLong idGenerator, Provider<ReentrantReadWriteLock> lockProvider) {
        this.idGenerator = idGenerator;
        this.lockProvider = lockProvider;
    }

    public Account createNewAccount() {
        return new Account(idGenerator.incrementAndGet(), lockProvider.get());
    }

    public Account createNewAccountWithBalance(BigDecimal amount) {
        return new Account(idGenerator.incrementAndGet(), amount, lockProvider.get());
    }
}
