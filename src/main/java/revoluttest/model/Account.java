package revoluttest.model;

import com.google.inject.Inject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Account {

    private final long accountId;
    private BigDecimal balance;
    private final Lock balanceReadLock;
    private final Lock balanceWriteLock;

    @Inject
    public Account(long accountId, ReentrantReadWriteLock readWriteLock) {
        this(accountId, BigDecimal.ZERO, readWriteLock);
    }

    @Inject
    public Account(long accountId, BigDecimal balance, ReentrantReadWriteLock readWriteLock) {
        this.accountId = accountId;
        this.balance = balance;
        balanceReadLock = readWriteLock.readLock();
        balanceWriteLock = readWriteLock.writeLock();
    }

    public long getAccountId() {
        return accountId;
    }

    public BigDecimal getBalance() {
        balanceReadLock.lock();
        try {
            return balance.setScale(2, RoundingMode.FLOOR);
        } finally {
            balanceReadLock.unlock();
        }
    }

    public void addToBalance(BigDecimal amountToAdd) {
        balanceWriteLock.lock();
        try {
            balance = balance.add(amountToAdd);
        } finally {
            balanceWriteLock.unlock();
        }
    }

    public void takeFromBalance(BigDecimal amountToTake) {
        balanceWriteLock.lock();
        try {
            balance = balance.subtract(amountToTake);
        } finally {
            balanceWriteLock.unlock();
        }
    }
}
