package revoluttest.service;

import com.google.inject.Inject;
import revoluttest.dao.AccountDao;
import revoluttest.exception.NoAccountForIdException;
import revoluttest.model.Account;

import java.math.BigDecimal;

public class AccountService {

    private final AccountDao accountDao;

    @Inject
    public AccountService(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    public long createAccount() {
        return accountDao.createNewAccount();
    }

    public long createAccountWithBalance(BigDecimal amount) {
        return accountDao.createNewAccountWithBalance(amount);
    }

    public void depositIntoAccount(long accountId, BigDecimal amount) throws NoAccountForIdException {
        System.out.println("Depositing amount "+amount+" into Account "+accountId);
        accountDao.depositIntoAccount(accountId, amount);
    }

    public void withdrawFromAccount(long accountId, BigDecimal amount) throws NoAccountForIdException {
        System.out.println("Withdrawing amount "+amount+" from Account "+accountId);
        accountDao.withdrawFromAccount(accountId, amount);
    }

    public void deleteAccount(long accountId) {
        System.out.println("Deleting Account "+accountId);
        accountDao.deleteAccount(accountId);
    }

    public BigDecimal getAccountBalance(long accountId) throws NoAccountForIdException {
        return accountDao.getAccount(accountId).getBalance();
    }

    public void transferBetweenAccounts(long fromAccountId, long toAccountId, BigDecimal amount) throws NoAccountForIdException {
        System.out.println("Request to transfer amount "+amount+", from Account "+fromAccountId+
                " to Account "+toAccountId);
        transferBetweenAccounts(accountDao.getAccount(fromAccountId), accountDao.getAccount(toAccountId), amount);
    }

    private void transferBetweenAccounts(Account fromAccount, Account toAccount, BigDecimal amount) {
        System.out.println("Transferring money from Account "+fromAccount.getAccountId()+
                " to Account"+toAccount.getAccountId());
        fromAccount.takeFromBalance(amount);
        toAccount.addToBalance(amount);
    }
}
