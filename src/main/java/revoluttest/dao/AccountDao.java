package revoluttest.dao;

import com.google.inject.Inject;
import revoluttest.exception.NoAccountForIdException;
import revoluttest.model.Account;
import revoluttest.model.factory.AccountFactory;

import java.math.BigDecimal;
import java.util.concurrent.ConcurrentHashMap;

public class AccountDao {

    private final ConcurrentHashMap<Long, Account> accountMap;
    private final AccountFactory accountFactory;

    @Inject
    public AccountDao(ConcurrentHashMap<Long, Account> accountMap, AccountFactory accountFactory) {
        this.accountMap = accountMap;
        this.accountFactory = accountFactory;
    }

    public long createNewAccount() {
        final Account newAccount = accountFactory.createNewAccount();
        final long accountKey = newAccount.getAccountId();
        accountMap.put(accountKey, newAccount);
        return accountKey;
    }

    public long createNewAccountWithBalance(BigDecimal amount) {
        final Account newAccount = accountFactory.createNewAccountWithBalance(amount);
        final long accountKey = newAccount.getAccountId();
        accountMap.put(accountKey, newAccount);
        return accountKey;
    }

    public Account getAccount(long id) throws NoAccountForIdException {
        final Account account = accountMap.get(id);
        if(account == null) {
            throw new NoAccountForIdException("No Account found for ID: "+id);
        }
        return account;
    }

    public void depositIntoAccount(long accountId, BigDecimal amount) throws NoAccountForIdException {
        Account account = getAccount(accountId);
        account.addToBalance(amount);
    }

    public void withdrawFromAccount(long accountId, BigDecimal amount) throws NoAccountForIdException {
        Account account = getAccount(accountId);
        account.takeFromBalance(amount);
    }

    public void deleteAccount(long accountId) {
        Account removed = accountMap.remove(accountId);
        System.out.println("Removed Account: "+removed+", with Id: "+accountId);
    }
}
