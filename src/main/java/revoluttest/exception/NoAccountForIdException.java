package revoluttest.exception;

public class NoAccountForIdException extends Exception {

    public NoAccountForIdException(String message) {
        super(message);
    }

}
