import com.google.inject.Guice;
import com.google.inject.Injector;
import revoluttest.exception.NoAccountForIdException;
import revoluttest.service.AccountService;

import java.math.BigDecimal;

public class BlahMain {

    public static void main(String[] args) throws NoAccountForIdException {
        Injector injector = Guice.createInjector();
        AccountService accountService = injector.getInstance(AccountService.class);
        long accountId1 = accountService.createAccountWithBalance(BigDecimal.valueOf(3.456));
        long accountId2 = accountService.createAccount();
        printAccountInfo(accountService, accountId1, accountId2);
        accountService.transferBetweenAccounts(accountId1, accountId2, BigDecimal.valueOf(2.5));
        printAccountInfo(accountService, accountId1, accountId2);


        long nonExistentAccountId = 3L;
        try {
            accountService.transferBetweenAccounts(accountId1, nonExistentAccountId, BigDecimal.ONE);
        } catch (NoAccountForIdException e) {
            System.out.println("Got expected error: " + e);
        }
        printAccountInfo(accountService, accountId1, accountId2);
        try {
            accountService.transferBetweenAccounts(nonExistentAccountId, accountId2, BigDecimal.ONE);
        } catch (NoAccountForIdException e) {
            System.out.println("Got expected error: " + e);
        }
        printAccountInfo(accountService, accountId1, accountId2);
        accountService.transferBetweenAccounts(accountId1, accountId2, BigDecimal.valueOf(9.876));
        printAccountInfo(accountService, accountId1, accountId2);
        accountService.depositIntoAccount(accountId1, BigDecimal.TEN);
        printAccountInfo(accountService, accountId1, accountId2);
        accountService.withdrawFromAccount(accountId1, BigDecimal.ONE);
        printAccountInfo(accountService, accountId1, accountId2);
        accountService.deleteAccount(accountId1);
        accountService.deleteAccount(accountId1);
    }

    private static void printAccountInfo(AccountService accountService, long accountId1, long accountId2) {
        try {
            System.out.println("Account ID: " + accountId1 + "; Balance: " + accountService.getAccountBalance(accountId1));
            System.out.println("Account ID: " + accountId2 + "; Balance: " + accountService.getAccountBalance(accountId2));
        } catch (NoAccountForIdException ex) {
            System.out.println("Got unexpected error: " + ex);
        }
    }

}
