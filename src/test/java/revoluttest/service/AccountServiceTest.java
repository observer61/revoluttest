package revoluttest.service;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import revoluttest.dao.AccountDao;
import revoluttest.exception.NoAccountForIdException;
import revoluttest.model.Account;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AccountServiceTest {

    private AccountService accountService;
    @Mock
    private AccountDao accountDao;
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        accountService = new AccountService(accountDao);
    }

    @Test
    public void createAccountCallsCreateAccountOnAccountDao() {

        accountService.createAccount();

        verify(accountDao).createNewAccount();
    }

    @Test
    public void createAccountWithAmountCallsCreateAccountOnAccountDaoWithAmount() {
        BigDecimal amount = BigDecimal.valueOf(8);

        accountService.createAccountWithBalance(amount);

        verify(accountDao).createNewAccountWithBalance(amount);
    }

    @Test
    public void depositIntoAccountDepositsAmountIntoAccountViaDao() throws Exception {
        long accountId = 8L;
        BigDecimal amount = BigDecimal.valueOf(3);

        accountService.depositIntoAccount(accountId, amount);

        verify(accountDao).depositIntoAccount(accountId, amount);
    }

    @Test
    public void withdrawFromAccountDepositsAmountIntoAccountViaDao() throws Exception {
        long accountId = 8L;
        BigDecimal amount = BigDecimal.valueOf(3);

        accountService.withdrawFromAccount(accountId, amount);

        verify(accountDao).withdrawFromAccount(accountId, amount);
    }

    @Test
    public void deleteAccountDeletesAccountFromAccountDao() throws Exception {
        long accountId = 8L;

        accountService.deleteAccount(accountId);

        verify(accountDao).deleteAccount(accountId);
    }

    @Test
    public void getAccountBalanceReturnsBalanceOfAccountFromAccountDao() throws Exception {
        long accountId = 4L;
        Account account = mock(Account.class);
        BigDecimal expectedBalance = BigDecimal.valueOf(5);

        when(accountDao.getAccount(accountId)).thenReturn(account);
        when(account.getBalance()).thenReturn(expectedBalance);

        BigDecimal accountBalance = accountService.getAccountBalance(accountId);

        assertEquals(accountBalance, expectedBalance);
    }

    @Test
    public void getAccountBalanceThrowsSameExceptionWhenAccountDaoThrowsException() throws Exception {
        long accountId = 4L;
        String someMessage = "some message";
        expectedException.expect(NoAccountForIdException.class);
        expectedException.expectMessage(someMessage);

        when(accountDao.getAccount(accountId)).thenThrow(new NoAccountForIdException(someMessage));

        accountService.getAccountBalance(accountId);
    }

    @Test
    public void transferMoneyBetweenAccountsTakesAmountFromAccountOfFirstIdAndAddsItToAccountOfSecondId() throws Exception {
        BigDecimal amountToTransfer = BigDecimal.valueOf(3);
        long firstId = 1L, secondId = 2L;

        Account accountOfFirstId = mock(Account.class);
        Account accountOfSecondId = mock(Account.class);
        when(accountDao.getAccount(firstId)).thenReturn(accountOfFirstId);
        when(accountDao.getAccount(secondId)).thenReturn(accountOfSecondId);
        accountService.transferBetweenAccounts(firstId, secondId, amountToTransfer);

        verify(accountOfFirstId).takeFromBalance(amountToTransfer);
        verify(accountOfSecondId).addToBalance(amountToTransfer);
    }

    @Test
    public void transferMoneyBetweenAccountsTakesNoMoneyFromAccountOfFirstIdWhenSecondIdThrowsException() throws Exception {
        long firstId = 1L, secondId = 2L;
        expectedException.expect(NoAccountForIdException.class);

        Account accountOfId1 = mock(Account.class);
        when(accountDao.getAccount(firstId)).thenReturn(accountOfId1);
        when(accountDao.getAccount(secondId)).thenThrow(new NoAccountForIdException(""));
        accountService.transferBetweenAccounts(firstId, secondId, BigDecimal.ONE);

        verify(accountOfId1, never()).addToBalance(any());
    }

    @Test
    public void transferMoneyBetweenAccountsTakesAddsNoMoneyToAccountOfSecondIdWhenFirstIdThrowsException() throws Exception {
        long firstId = 1L, secondId = 2L;
        expectedException.expect(NoAccountForIdException.class);

        Account accountOfSecondId = mock(Account.class);
        when(accountDao.getAccount(firstId)).thenThrow(new NoAccountForIdException(""));
        when(accountDao.getAccount(secondId)).thenReturn(accountOfSecondId);
        accountService.transferBetweenAccounts(firstId, secondId, BigDecimal.ONE);

        verify(accountOfSecondId, never()).addToBalance(any());
    }
}