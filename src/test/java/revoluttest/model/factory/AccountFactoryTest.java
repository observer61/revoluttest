package revoluttest.model.factory;

import com.google.inject.Provider;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import revoluttest.model.Account;

import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AccountFactoryTest {

    private AccountFactory accountFactory;
    @Mock
    private AtomicLong idGenerator;
    @Mock
    private Provider<ReentrantReadWriteLock> lockProvider;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        accountFactory = new AccountFactory(idGenerator, lockProvider);
        when(lockProvider.get()).thenReturn(mock(ReentrantReadWriteLock.class));
    }

    @Test
    public void createNewAccountUsesGeneratedIdToSetAccountId() {
        accountFactory = new AccountFactory(idGenerator, lockProvider);

        Account newAccount = accountFactory.createNewAccountWithBalance(BigDecimal.ONE);
        long generatedId = idGenerator.get();

        assertEquals(generatedId, newAccount.getAccountId());
    }

    @Test
    public void createNewAccounteUsesProvidedLockToGenerateReadLockAndWriteLock() {
        ReentrantReadWriteLock lock = mock(ReentrantReadWriteLock.class);

        when(lockProvider.get()).thenReturn(lock);
        accountFactory.createNewAccount();

        verify(lock).readLock();
        verify(lock).writeLock();
    }

    @Test
    public void createNewAccountWithBalanceUsesGeneratedIdToSetAccountId() {
        accountFactory = new AccountFactory(idGenerator, lockProvider);

        Account newAccount = accountFactory.createNewAccount();
        long generatedId = idGenerator.get();

        assertEquals(generatedId, newAccount.getAccountId());
    }

    @Test
    public void createNewAccountWithBalanceUsesBalanceParameterToSetAccountBalance() {
        BigDecimal balance = BigDecimal.valueOf(4);

        when(lockProvider.get()).thenReturn(new ReentrantReadWriteLock());
        Account newAccount = accountFactory.createNewAccountWithBalance(balance);

        assertEquals(0, newAccount.getBalance().compareTo(balance));
    }

    @Test
    public void createNewAccountWithBalanceUsesProvidedLockToGenerateReadLockAndWriteLock() {
        ReentrantReadWriteLock lock = mock(ReentrantReadWriteLock.class);

        when(lockProvider.get()).thenReturn(lock);
        accountFactory.createNewAccountWithBalance(BigDecimal.ONE);

        verify(lock).readLock();
        verify(lock).writeLock();
    }

}