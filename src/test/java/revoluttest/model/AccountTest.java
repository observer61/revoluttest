package revoluttest.model;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class AccountTest {

    private Account account;
    private ReentrantReadWriteLock reentrantReadWriteLock = mock(ReentrantReadWriteLock.class);
    @Mock
    private ReentrantReadWriteLock.ReadLock readLock;
    @Mock
    private ReentrantReadWriteLock.WriteLock writeLock;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        when(reentrantReadWriteLock.readLock()).thenReturn(readLock);
        when(reentrantReadWriteLock.writeLock()).thenReturn(writeLock);
    }

    @Test
    public void getAccountIdReturnsAccountIdFromConstructorWithoutBalance() {
        long expectedAccountId = 13L;
        account = new Account(expectedAccountId, reentrantReadWriteLock);

        long resultingAccountId = account.getAccountId();

        assertEquals(expectedAccountId, resultingAccountId);
    }

    @Test
    public void getAccountIdReturnsAccountIdFromConstructorWithBalance() {
        long expectedAccountId = 11L;
        account = new Account(expectedAccountId, null, reentrantReadWriteLock);

        long resultingAccountId = account.getAccountId();

        assertEquals(expectedAccountId, resultingAccountId);
    }

    @Test
    public void getBalanceReturnsZeroFromConstructorWithoutBalance() {
        account = new Account(1, reentrantReadWriteLock);

        BigDecimal resultingBalance = account.getBalance();

        BigDecimal expectedBalance = BigDecimal.ZERO.setScale(2, RoundingMode.FLOOR);
        assertEquals(expectedBalance, resultingBalance);
    }

    @Test
    public void getBalanceReturnsBalanceFromConstructorWithBalance() {
        BigDecimal balance = BigDecimal.valueOf(7);
        account = new Account(1, balance, reentrantReadWriteLock);

        BigDecimal resultingBalance = account.getBalance();

        BigDecimal expectedBalance = balance.setScale(2, RoundingMode.FLOOR);
        assertEquals(expectedBalance, resultingBalance);
    }

    @Test
    public void getBalanceLocksReadLockBeforeReturningBalanceAndUnlocksReadLockAfter() {
        BigDecimal balance = mock(BigDecimal.class);
        account = new Account(1, balance, reentrantReadWriteLock);

        account.getBalance();

        InOrder inOrder = inOrder(readLock, balance);
        inOrder.verify(readLock).lock();
        inOrder.verify(balance).setScale(anyInt(), any(RoundingMode.class));
        verifyNoMoreInteractions(balance);
        inOrder.verify(readLock).unlock();
    }

    @Test
    public void addToBalanceLocksWriteLockBeforeAddingToBalanceAndUnlocksWriteLockAfter() {
        BigDecimal balance = mock(BigDecimal.class);
        account = new Account(1, balance, reentrantReadWriteLock);

        account.addToBalance(BigDecimal.ONE);

        InOrder inOrder = inOrder(writeLock, balance);
        inOrder.verify(writeLock).lock();
        inOrder.verify(balance).add(any(BigDecimal.class));
        verifyNoMoreInteractions(balance);
        inOrder.verify(writeLock).unlock();
    }

    @Test
    public void addToBalanceMakesValueOfGetBalanceEqualToInitialBalancePlusAddedAmount() {
        int balance1Value = 3, balance2Value = 5;
        account = new Account(1, BigDecimal.valueOf(balance1Value), reentrantReadWriteLock);

        account.addToBalance(BigDecimal.valueOf(balance2Value));
        BigDecimal resultingBalance = account.getBalance();

        BigDecimal expectedBigDecimal = BigDecimal.valueOf(balance1Value + balance2Value);//.setScale(2,RoundingMode.FLOOR);
        assertEquals(0, expectedBigDecimal.compareTo(resultingBalance));
    }

    @Test
    public void takeFromBalanceLocksWriteLockBeforeSubtractingBalanceAndUnlocksWriteLockAfter() {
        BigDecimal balance = mock(BigDecimal.class);
        account = new Account(1, balance, reentrantReadWriteLock);

        account.takeFromBalance(BigDecimal.ONE);

        InOrder inOrder = inOrder(writeLock, balance);
        inOrder.verify(writeLock).lock();
        inOrder.verify(balance).subtract(any(BigDecimal.class));
        verifyNoMoreInteractions(balance);
        inOrder.verify(writeLock).unlock();
    }

    @Test
    public void takeFromBalanceMakesValueOfGetBalanceEqualToInitialBalanceMinusTakenAmount() {
        int balance1Value = 3, balance2Value = 5;
        account = new Account(1, BigDecimal.valueOf(balance1Value), reentrantReadWriteLock);

        account.takeFromBalance(BigDecimal.valueOf(balance2Value));
        BigDecimal resultingBalance = account.getBalance();

        BigDecimal expectedBigDecimal = BigDecimal.valueOf(balance1Value - balance2Value);//.setScale(2,RoundingMode.FLOOR);
        assertEquals(0, expectedBigDecimal.compareTo(resultingBalance));
    }
}