package revoluttest.dao;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import revoluttest.exception.NoAccountForIdException;
import revoluttest.model.Account;
import revoluttest.model.factory.AccountFactory;

import java.math.BigDecimal;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.longThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AccountDaoTest {

    private AccountDao accountDao;
    @Mock
    private ConcurrentHashMap<Long, Account> concurrentHashMap;
    @Mock
    private AccountFactory accountFactory;
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        accountDao = new AccountDao(concurrentHashMap, accountFactory);
    }

    @Test
    public void createNewAccountAddsAccountFromFactoryToMapWithItsIdAsTheKey() {
        long accountId = 12L;
        Account account = new Account(accountId, new ReentrantReadWriteLock());

        when(accountFactory.createNewAccount()).thenReturn(account);
        accountDao.createNewAccount();

        verify(concurrentHashMap).put(accountId, account);
    }

    @Test
    public void createNewAccountWithBalanceAddsAccountFromFactoryToMapWithItsIdAsTheKey() {
        long accountId = 1L;
        Account account = new Account(accountId, new ReentrantReadWriteLock());

        when(accountFactory.createNewAccountWithBalance(any())).thenReturn(account);
        accountDao.createNewAccountWithBalance(BigDecimal.ONE);

        verify(concurrentHashMap).put(accountId, account);
    }

    @Test
    public void getAccountReturnsAccountMappedToId() throws NoAccountForIdException {
        long id = 4L;
        Account accountFromMap = new Account(1, new ReentrantReadWriteLock());

        when(concurrentHashMap.get(id)).thenReturn(accountFromMap);
        Account account = accountDao.getAccount(id);

        assertEquals(accountFromMap, account);
    }

    @Test
    public void getAccountThrowsNoAccountForIdExceptionWithMessageContainingIdWhenNoAccountMappedToId() throws NoAccountForIdException {
        long id = 4L;
        expectedException.expect(NoAccountForIdException.class);
        expectedException.expectMessage(String.valueOf(id));

        when(concurrentHashMap.get(id)).thenReturn(null);
        accountDao.getAccount(id);
    }

    @Test
    public void depositIntoAccountDepositsAmountIntoAccountViaDao() throws Exception {
        long id = 7L;
        BigDecimal amount = BigDecimal.valueOf(2);
        Account account = mock(Account.class);

        when(concurrentHashMap.get(id)).thenReturn(account);
        accountDao.depositIntoAccount(id, amount);

        verify(account).addToBalance(amount);
    }

    @Test
    public void depositIntoAccountThrowsNoAccountForIdExceptionWithMessageContainingIdWhenNoAccountMappedToId() throws NoAccountForIdException {
        long id = 72L;
        expectedException.expect(NoAccountForIdException.class);
        expectedException.expectMessage(String.valueOf(id));

        when(concurrentHashMap.get(id)).thenReturn(null);
        accountDao.depositIntoAccount(id, BigDecimal.ONE);
    }

    @Test
    public void withdrawFromAccountDepositsAmountIntoAccountViaDao() throws Exception {
        long id = 7L;
        BigDecimal amount = BigDecimal.valueOf(2);
        Account account = mock(Account.class);

        when(concurrentHashMap.get(id)).thenReturn(account);
        accountDao.withdrawFromAccount(id, amount);

        verify(account).takeFromBalance(amount);
    }

    @Test
    public void withdrawFromAccountThrowsNoAccountForIdExceptionWithMessageContainingIdWhenNoAccountMappedToId() throws NoAccountForIdException {
        long id = 72L;
        expectedException.expect(NoAccountForIdException.class);
        expectedException.expectMessage(String.valueOf(id));

        when(concurrentHashMap.get(id)).thenReturn(null);
        accountDao.withdrawFromAccount(id, BigDecimal.ONE);
    }

    @Test
    public void deleteAccountRemovesAccountFromMap() throws Exception {
        long id = 7L;

        accountDao.deleteAccount(id);

        verify(concurrentHashMap).remove(id);
    }

}